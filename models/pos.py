# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import fields, models, api, _
from datetime import datetime, timedelta
from odoo.exceptions import Warning
import time
from pytz import timezone
from odoo.tools import float_is_zero

class PosOrder(models.Model):
    _inherit = "pos.order"

    user_id = fields.Many2one(
        comodel_name='res.users', string='Cashier',
        help="Person who uses the cash register. It can be a reliever, a student or an interim employee.",
        default=lambda self: self.env.uid,
        states={'done': [('readonly', True)], 'invoiced': [('readonly', True)]},
    )
    salesman_id = fields.Many2one('res.users',string='Salesman')

    def create(self, values):
        order_id = super(PosOrder, self).create(values)
        if not order_id.user_id.has_group('point_of_sale.group_pos_manager'):
            config_obj = self.env['pos.config'].search([])
            notifications = []
            for each in config_obj:
                if each.sale_note_users:
                    for user in each.sale_note_users:
                        if user.id == order_id.user_id.id:
                            session = self.env['pos.session'].search([('config_id','=',each.id)], limit=1)
                            if session:
                                notifications.append(
                                    ((self._cr.dbname, 'sale.note', session.user_id.id), ('new_pos_order', order_id.read())))
                                self.env['bus.bus'].sendmany(notifications)
        return order_id

    def _order_fields(self,ui_order):
        res = super(PosOrder, self)._order_fields(ui_order)
        res.update({
            'salesman_id':ui_order.get('salesman_id') or False,
        });
        return res

    @api.multi
    def unlink(self):
        notifications = []
        notify_users = []
        for order in self:
            order_user = self.env['res.users'].browse(order.user_id.id)
            if order.salesman_id:
                if order._uid == order.salesman_id.id:
                    config_obj = self.env['pos.config'].search([])
                    for each in config_obj:
                        if each.sale_note_users:
                            for user in each.sale_note_users:
                                if user.id == order_user.id:
                                    session = self.env['pos.session'].search([('config_id', '=', each.id)], limit=1)
                                    if session:
                                        notify_users.append(session.user_id.id)
                else:
                    notify_users.append(order.salesman_id.id)
                for user in notify_users:
                    notifications.append(((self._cr.dbname, 'sale.note', user),
                                              ('cancelled_sale_note', order.read())))
                self.env['bus.bus'].sendmany(notifications)
        return super(PosOrder, self).unlink()

    @api.multi
    def write(self, vals):
        res = super(PosOrder, self).write(vals)
        notifications = []
        notify_users = []
        order_id = self.browse(vals.get('old_order_id'))
        order_user = self.env['res.users'].browse(vals.get('user_id'))
#         if order_id.salesman_id:
#             notify_users.append(order_id.salesman_id.id)
        config_obj = self.env['pos.config'].search([])
        for each in config_obj:
            if each.sale_note_users:
                for user in each.sale_note_users:
                    if user.id == order_user.id:
                        session = self.env['pos.session'].search([('config_id', '=', each.id)], limit=1)
                        if session:
                            notify_users.append(session.user_id.id)
        for user in notify_users:
            notifications.append(((self._cr.dbname, 'sale.note', user),
                                      ('new_pos_order', order_id.read())))
        self.env['bus.bus'].sendmany(notifications)
        return res

    @api.multi
    def action_pos_order_paid(self):
        if not self.test_paid():
            raise UserError(_("Order is not paid."))
        self.write({'state': 'paid'})
        notifications_for_products =[]
        notifications = []
        notify_users = []
#         for order_id in self:
        order_id = self
        order_user = self.env['res.users'].browse(order_id.user_id.id)
        if order_id.salesman_id:
            notify_users.append(order_id.salesman_id.id)
    #         if not order_user.has_group('point_of_sale.group_pos_manager'):
        config_obj = self.env['pos.config'].search([])
        for each in config_obj:
            if each.sale_note_users:
                for user in each.sale_note_users:
                    if user.id == order_user.id:
                        session = self.env['pos.session'].search([('config_id', '=', each.config_id)], limit=1)
                        if session:
                            notify_users.append(order_user.id)
            orderlines = []
            for line in order_id.lines:
                orderlines.append(line.read())
            session_id = self.env['pos.session'].search([('config_id','=',each.id),('state','=','opened')],limit=1)
            if session_id and session_id.user_id:
                notifications_for_products.append(((self._cr.dbname, 'sale.note', session_id.user_id.id),
                                  ('update_product_screen', orderlines)))
                self.env['bus.bus'].sendmany(notifications_for_products)
        if len(notify_users) > 0:
            for user in notify_users:
                notifications.append(((self._cr.dbname, 'sale.note', user),
                                          ('new_pos_order', order_id.read())))
            self.env['bus.bus'].sendmany(notifications)
        return self.create_picking()

    @api.model
    def _process_order(self,order):
        pos_line_obj = self.env['pos.order.line']
        draft_order_id = order.get('old_order_id')
        if order.get('draft_order'):
            if not draft_order_id:
                order.pop('draft_order')
                order_id = self.create(self._order_fields(order))
                return order_id
            else:
                order_id = draft_order_id
                pos_line_ids = pos_line_obj.search([('order_id', '=', order_id)])
                if pos_line_ids:
                    pos_line_obj.unlink(pos_line_ids)
                self.write([order_id],
                           {'lines': order['lines'],
                            'partner_id': order.get('partner_id')})
                return order_id

        if not order.get('draft_order') and draft_order_id:
            order_id = draft_order_id
            order_obj = self.browse(order_id)
            pos_line_ids = pos_line_obj.search([('order_id', '=', order_id)])
            if pos_line_ids:
                for line_id in pos_line_ids:
                    line_id.unlink()
            temp = order.copy()
            temp.pop('statement_ids', None)
            temp.pop('name', None)
            temp.update({
                'date_order': order.get('creation_date')
            })
            order_obj.write(temp)
            for payments in order['statement_ids']:
                order_obj.add_payment(self._payment_fields(payments[2]))

            session = self.env['pos.session'].browse(order['pos_session_id'])
            if session.sequence_number <= order['sequence_number']:
                session.write({'sequence_number': order['sequence_number'] + 1})
                session.refresh()

            if not float_is_zero(order['amount_return'], self.env['decimal.precision'].precision_get('Account')):
                cash_journal = session.cash_journal_id
                if not cash_journal:
                    cash_journal_ids = session.statement_ids.filtered(lambda st: st.journal_id.type == 'cash')
                    if not len(cash_journal_ids):
                        raise Warning(_('error!'),
                                             _("No cash statement found for this session. Unable to record returned cash."))
                    cash_journal = cash_journal_ids[0].journal_id
                order_obj.add_payment({
                    'amount': -order['amount_return'],
                    'payment_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'payment_name': _('return'),
                    'journal': cash_journal.id,
                })
            return order_obj

        if not order.get('draft_order') and not draft_order_id:
            order_id = super(PosOrder, self)._process_order(order)
            return order_id

    @api.model
    def ac_pos_search_read(self, domain):
        domain = domain.get('domain')
        search_vals = self.search_read(domain)
        user_id = self.env['res.users'].browse(self._uid)
        tz = False
        result = []
        if self._context and self._context.get('tz'):
            tz = timezone(self._context.get('tz'))
        elif user_id and user_id.tz:
            tz = timezone(user_id.tz)
        if tz:
            c_time = datetime.now(tz)
            hour_tz = int(str(c_time)[-5:][:2])
            min_tz = int(str(c_time)[-5:][3:])
            sign = str(c_time)[-6][:1]
            for val in search_vals:
                if sign == '-':
                    val.update({
                        'date_order':(datetime.strptime(val.get('date_order'), '%Y-%m-%d %H:%M:%S') - timedelta(hours=hour_tz, minutes=min_tz)).strftime('%Y-%m-%d %H:%M:%S')
                    })
                elif sign == '+':
                    val.update({
                        'date_order':(datetime.strptime(val.get('date_order'), '%Y-%m-%d %H:%M:%S') + timedelta(hours=hour_tz, minutes=min_tz)).strftime('%Y-%m-%d %H:%M:%S')
                    })
                result.append(val)
            return result
        else:
            return search_vals


class pos_config(models.Model):
    _inherit = "pos.config"

    enable_reorder = fields.Boolean("Draft Order Management")
    sale_note_users = fields.Many2many('res.users',string="Sales Persons")
    enable_operation_restrict = fields.Boolean("Operation Restrict")
    pos_managers_ids = fields.Many2many('res.users','posconfig_partner_rel','location_id','partner_id', string='Managers')

class ResUsers(models.Model):
    _inherit = 'res.users'

    can_give_discount = fields.Boolean("Can Give Discount")
    can_change_price = fields.Boolean("Can Change Price")
    discount_limit = fields.Float("Discount Limit")
    based_on = fields.Selection([('pin','Pin'),('barcode','Barcode')],
                                   default='barcode',string="Authenticaion Based On")

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        if self._context and self._context.get('sale_note_users'):
            config_ids = self.env['pos.config'].search([('sale_note_users', '!=', False)])
            exsiting_user_ids = []
            for config in config_ids:
                for u in config.sale_note_users:
                    exsiting_user_ids.append(u.id)
            users = []
            managers = []
            pos_manager_id = self.env.ref('point_of_sale.group_pos_manager')
            pos_users_id = self.env.ref('point_of_sale.group_pos_user')
            for each in pos_manager_id.users:
                managers.append(each.id)
            for each_user in pos_users_id.users:
                if each_user.id not in exsiting_user_ids:
                    if each_user.id not in managers:
                        users.append(each_user.id)
            args += [('id', 'in', users)]
            res = super(ResUsers, self).search(args=args, offset=offset, limit=limit, order=order, count=count)
        else:
             res = super(ResUsers, self).search(args=args, offset=offset, limit=limit, order=order, count=count)
        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: